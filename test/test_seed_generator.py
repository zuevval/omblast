import numpy as np  # type: ignore
import unittest

from src import seed_generator as sg
from src.utils import OpticalMap


class TestSeedGenerator(unittest.TestCase):
    def test_ktuple_hash_functor(self):
        ktuple_hash_f = sg.ktuple_hash_functor(3.)
        ktuple = np.array([1., 2., 3., 4., 5., 6.])
        hash = ktuple_hash_f(ktuple)
        assert len(hash) == len(ktuple)
        assert hash[0] == hash[1]
        assert hash[2] == hash[3] == hash[4]
        assert hash[0] != hash[2] != hash[5]

    def test_ktuple_similarity_functor(self):
        ktuples_are_similar = sg.ktuple_similarity_functor(0, 0)
        assert ktuples_are_similar([1., 1., 1.], [1., 1., 1.])
        assert not ktuples_are_similar([1., 1., 1.], [1., 1.0001, 1.])

        ktuples_are_similar = sg.ktuple_similarity_functor(0, 0.1)
        assert ktuples_are_similar([1.], [1.])
        assert ktuples_are_similar([0.], [0.05])
        assert ktuples_are_similar([0.], [-0.1])
        assert not ktuples_are_similar([0.], [0.1001])

    def test_generate_seed_simple(self):
        ref_frags = np.array([1, 1, 1, 1])
        query_frags = np.array([1, 1, 1, 1])

        ref, query = OpticalMap(ref_frags), OpticalMap(query_frags)
        assert (ref.get_frags() == ref_frags).all()
        assert (query.get_frags() == query_frags).all()

        expected_alignments = [
            [(0, 0), (1, 1), (2, 2), (3, 3)],
            [(0, 1), (1, 2), (2, 3), (3, 4)],
            [(1, 0), (2, 1), (3, 2), (4, 3)],
            [(1, 1), (2, 2), (3, 3), (4, 4)]
        ]

        seeds = sg.generate_seeds(ref, query, sg.SeedGenParams())
        assert len(seeds) == len(expected_alignments)
        for seed in seeds:
            assert seed.cut_pairs in expected_alignments
