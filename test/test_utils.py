import unittest

from src.utils import OpticalMap, FwdOMapIterator


class TestUtils(unittest.TestCase):
    def test_optical_map_iterator(self):
        om = OpticalMap([1., 1., 1., 1.])
        om_it = FwdOMapIterator(om, 1)
        for idx in range(len(om.get_frags()) - 1):
            assert om_it.has_next()
            next_frag, next_cut = om_it.observe_next()
            frag, cut = om_it.next()
            assert next_frag == frag and next_cut == cut
        assert not om_it.has_next()
