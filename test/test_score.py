import unittest
from src.score import ScoreParams, score_partial
from src.utils import Alignment


class TestScore(unittest.TestCase):
    def test_partial_score(self):
        aln = Alignment([0, 1, 4, 5], [0, 1, 2, 4])  # 2 missing cuts, 1 extra cut
        params = ScoreParams(scale_factor=1, match_score=1, miss_penalty=.5, extra_penalty=.25)
        assert score_partial(aln, params) == 2.75
