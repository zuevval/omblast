import unittest

from src.seed_extender import extend_aln, ExtensionParams, extend_with_refinement, RefinedExtensionParams
from src.utils import OpticalMap, FwdOMapIterator, Alignment


class TestSeedExtender(unittest.TestCase):
    def test_extend_aln_simple(self):
        ref, query = OpticalMap([1., 1., 1., 1.]), OpticalMap([1., 1., 1., 1.])
        ref_it, query_it = FwdOMapIterator(ref, 1), FwdOMapIterator(query, 1)
        extension = extend_aln(ref_it, query_it, ExtensionParams())
        assert len(extension) == 3

    def test_extend_seeds(self):
        ref, query = OpticalMap([1., 1., 1., 1.]), OpticalMap([1., 1., 1., 1.])
        seeds = [
            Alignment([0, 1], [1, 2]),
            Alignment([1], [3]),
            Alignment([1], [4]),
            Alignment([2, 3], [2, 3])
        ]
        extended = [aln.cut_pairs for [aln, _] in extend_with_refinement(ref, query, seeds, RefinedExtensionParams())]

        expected_extensions = [
            [(0, 1), (1, 2), (2, 3), (3, 4)],
            [(0, 2), (1, 3), (2, 4)],
            [(0, 3), (1, 4)],
            [(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)]
        ]
        assert len(extended) == len(expected_extensions)
        for aln_frags in extended:
            assert aln_frags in expected_extensions
