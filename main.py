import numpy as np  # type: ignore
import logging
import sys

from src import seed_generator as sg, seed_extender as se, utils, score


def run_pipeline():
    data_dir = "test/data/artificial_trivial/"
    ref_frags, query_frags = np.fromfile(data_dir + "ref.txt", sep=" "), np.fromfile(data_dir + "query.txt", sep=" ")
    ref, query = utils.OpticalMap(ref_frags), utils.OpticalMap(query_frags)
    seeds = sg.generate_seeds(ref, query, sg.SeedGenParams(n_bins=10))
    extended = se.extend_with_refinement(ref, query, seeds,
                                         se.RefinedExtensionParams(min_scale=0.9, max_scale=1.1, n_scales=5))
    logging.debug([aln.cut_pairs for aln, _ in extended])
    logging.debug(score.score_partial(extended[0][0], score.ScoreParams()))


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    run_pipeline()
