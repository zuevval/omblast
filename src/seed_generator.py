import logging
from collections import defaultdict
from dataclasses import dataclass
from typing import List, Callable
import numpy as np  # type: ignore

from src.utils import OpticalMap, Alignment


@dataclass
class SeedGenParams:
    ktuple_len: int = 3  # length  of k-tuples used for seeding (in fragments)
    measurement_tol: float = 0.  # length measurement tolerance
    scale_tol: float = 0.  # optical map scaling tolerance
    n_bins: int = 500  # number of bins for k-tuple sizes in binning algorithm


def ktuple_hash_functor(bin_size: float) -> Callable[[np.array], str]:
    def ktuple_hash(frags: np.array) -> str:
        char_codes = [int(frag / bin_size) for frag in frags]
        return ''.join(map(chr, char_codes))  # make a string from character codes

    return ktuple_hash


def ktuple_similarity_functor(scale_tol: float, measurement_tol: float) -> Callable[[np.array, np.array], bool]:
    def ktuples_are_similar(ref_frags: np.array, query_frags: np.array) -> bool:
        for r_frag, q_frag in zip(ref_frags, query_frags):
            lower_bound = r_frag * (1 - scale_tol) - measurement_tol
            upper_bound = r_frag * (1 + scale_tol) + measurement_tol
            if not (q_frag >= lower_bound or np.isclose(lower_bound, q_frag)) \
                    or not (q_frag <= upper_bound or np.isclose(upper_bound, q_frag)):
                return False
        return True

    return ktuples_are_similar


def generate_seeds(ref: OpticalMap, query: OpticalMap, params: SeedGenParams) -> List[Alignment]:
    # calculating hashes for k-tuples on the query and storing them in hash-table (dictionary)
    ref_frags, query_frags = ref.get_frags(), query.get_frags()
    max_frag_len = np.max((np.max(ref_frags), np.max(query_frags)))
    bin_size = max_frag_len / params.n_bins
    logging.debug(bin_size)
    ktuple_hash: Callable[[np.array], str] = ktuple_hash_functor(bin_size)
    query_hashes = defaultdict(list)  # key: ktuple hash, value: list of start indices of frags
    for i_frag in range(len(query_frags) - params.ktuple_len + 1):
        q_ktuple = query_frags[i_frag:i_frag + params.ktuple_len]
        query_hashes[ktuple_hash(q_ktuple)].append(i_frag)

    # calculating hashes for k-tuples on the reference and searching that hashes in hash-table
    seeds: List[Alignment] = []
    ktuples_are_similar = ktuple_similarity_functor(scale_tol=params.scale_tol, measurement_tol=params.measurement_tol)
    for i_frag in range(len(ref_frags) - params.ktuple_len + 1):
        r_ktuple = ref_frags[i_frag:i_frag + params.ktuple_len]
        for query_i_frag in query_hashes[ktuple_hash(r_ktuple)]:
            q_ktuple = query_frags[query_i_frag:query_i_frag + params.ktuple_len]
            if not ktuples_are_similar(r_ktuple, q_ktuple):
                continue
            # if aligned fragments = [0, 1, 2] to [1, 2, 3], then aligned cuts = [0, 1, 2, 3] to [1, 2, 3, 4]
            ref_cut_ids = range(i_frag, i_frag + params.ktuple_len + 1)
            query_cut_ids = range(query_i_frag, query_i_frag + params.ktuple_len + 1)
            seeds.append(Alignment(ref_cut_ids, query_cut_ids))

    return seeds
