import logging
import sys
from typing import List
import numpy as np  # type: ignore


def read_contigs_from_cmap(filename: str) -> List[List[float]]:
    valid_row_n_cols = 10  # number of columns in a valid data row
    with open(filename) as fin:
        map_id = None
        maps_cuts: List[List[float]] = []
        for line in fin:
            if line[0] == '#':
                continue
            data_line: List[str] = line.strip().split('\t')
            if len(data_line) != valid_row_n_cols:
                continue
            row_id = int(data_line[0])
            if map_id != row_id:
                map_id = row_id
                maps_cuts.append([])
            maps_cuts[-1].append(float(data_line[5]))
    return maps_cuts


def cuts_to_frags(cuts: List[float]) -> np.array:
    if not cuts:
        return np.zeros(0)
    res = np.zeros(len(cuts) - 1)
    for i in range(len(cuts) - 1):
        res[i] = cuts[i + 1] - cuts[i]
    return res


def frags_from_file(filename: str) -> List[np.array]:
    cuts: List[List[float]] = read_contigs_from_cmap(filename)
    frags = [cuts_to_frags(c) for c in cuts]
    return frags


if __name__ == "__main__":
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    logging.debug(frags_from_file("../test/data/human_cmap/SAMN00004626.cmap")[0])
    logging.debug(frags_from_file("../test/data/human_cmap/SAMN00004627.cmap")[0])
