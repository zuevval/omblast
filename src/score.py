from dataclasses import dataclass

import numpy as np  # type: ignore

from src.utils import Alignment


@dataclass
class ScoreParams:
    scale_factor: float = 1
    match_score: float = 1  # bonus for each matched pair
    miss_penalty: float = .5  # penalty for a miss-cut on query map
    extra_penalty: float = .5  # penalty for an extra cut on query map


def score_partial(aln: Alignment, params: ScoreParams):
    pairs = aln.cut_pairs
    n_extra = n_missing = 0
    for idx in range(len(pairs) - 1):
        [r_cut, q_cut] = pairs[idx]  # cuts indices
        [r_next_cut, q_next_cut] = pairs[idx + 1]
        n_extra += q_next_cut - q_cut - 1
        n_missing += r_next_cut - r_cut - 1
    n_matched = len(pairs)

    return (params.match_score * n_matched - params.extra_penalty * n_extra - params.miss_penalty * n_missing) * (
        1 - np.abs(1 - params.scale_factor))
