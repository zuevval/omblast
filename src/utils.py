from typing import List, Tuple, Union

import numpy as np  # type: ignore


class OpticalMap:
    _frags: np.array  # lengths of fragments in kbp

    def __init__(self, frags: np.array):
        self._frags = frags

    def get_frags(self) -> np.array:
        return self._frags

    # TODO def get_cuts(self) -> np.array


class OpticalMapIterator:
    def has_next(self):
        raise NotImplementedError

    def next(self) -> Tuple[float, int]:
        """
        moves to next fragment on the map
        :return: fragment length & index of cut after fragment (or before if iterator is reverse)
        """
        raise NotImplementedError

    def observe_next(self) -> Tuple[float, int]:
        """
        doesn't move to next fragment
        :return: next fragment length & index of cut after it (or before if iterator is reverse)
        """
        raise NotImplementedError

    def __init__(self, o_map: OpticalMap, i_start: int):
        frags = o_map.get_frags()
        assert 0 <= i_start <= len(frags), "i_start must be a valid cut index"
        self._frags, self._index = frags, i_start


class FwdOMapIterator(OpticalMapIterator):
    def has_next(self):
        return self._index < len(self._frags)

    def next(self) -> Tuple[float, int]:
        if self._index == len(self._frags):
            raise IndexError
        self._index += 1
        return self._frags[self._index - 1], self._index

    def observe_next(self) -> Tuple[float, int]:
        if self._index == len(self._frags):
            raise IndexError
        return self._frags[self._index], self._index + 1


class BackOMapIterator(OpticalMapIterator):
    def has_next(self):
        return self._index > 0

    def next(self) -> Tuple[float, int]:
        if self._index == 0:
            raise IndexError
        self._index -= 1
        return self._frags[self._index], self._index

    def observe_next(self) -> Tuple[float, int]:
        if self._index == 0:
            raise IndexError
        return self._frags[self._index + 1], self._index


class Alignment:
    cut_pairs: List[Tuple[int, int]]  # first: reference cut index, second: query cut index

    def __init__(self, ref_cut_ids: np.array, query_cut_ids: np.array):
        self.cut_pairs = list(zip(ref_cut_ids, query_cut_ids))

    @staticmethod
    def from_cut_pairs(cut_pairs: List[Tuple[int, int]]) -> 'Alignment':
        result = Alignment([], [])
        result.cut_pairs = cut_pairs
        return result
