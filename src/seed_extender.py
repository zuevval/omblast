import logging
from dataclasses import dataclass
from typing import List, Tuple, Callable, Union, Any
import numpy as np  # type: ignore

from src import score
from src.utils import OpticalMap, OpticalMapIterator, FwdOMapIterator, Alignment, BackOMapIterator


@dataclass
class ExtensionParams:
    measurement_tol: float = 0
    max_miss_cuts: int = 2  # maximum acceptable number of non-matched cuts in a missed cuts sequence
    max_mismatch: int = 5  # maximum acceptable number of false cuts / missed cuts sequences in a row


@dataclass
class RefinedExtensionParams:
    extension_params: ExtensionParams = ExtensionParams()
    score_params: score.ScoreParams = score.ScoreParams()
    min_scale: float = 1
    max_scale: float = 1
    n_scales: int = 1


def frags_match_functor(measurement_tol: float) -> Callable[[float, float], bool]:
    def frags_do_match(ref_frag_len: float, query_frag_len: float):
        lower_bound = query_frag_len - measurement_tol
        upper_bound = query_frag_len + measurement_tol
        if lower_bound <= ref_frag_len <= upper_bound \
                or np.isclose(np.min([np.abs(lower_bound - ref_frag_len), np.abs(upper_bound - ref_frag_len)]), 0):
            return True
        return False

    return frags_do_match


def find_missed_cut(big_frag: float, other_frag: float, other_map_it: OpticalMapIterator) -> \
        Tuple[float, Union[int, None]]:
    """
    |-------big_frag-------|
    |-other_frag-|-------|-----|-|
                    ^
              other_map_it.next() (points to the next fragment after other_frag)

    :return: None if can't extend other_map;
    otherwise - index of cut matching last cut of big_frag on the other_map & accumulated length of fragments
    """
    cut_idx = None
    accumulated_frag_len = other_frag
    while other_map_it.has_next():
        next_frag_len, next_cut_idx = other_map_it.observe_next()
        if np.abs(big_frag - accumulated_frag_len) < np.abs(
                big_frag - accumulated_frag_len - next_frag_len):
            # if current other_map cut is closer than next to cut bounding the big_frag
            break
        other_map_it.next()  # next cut is closer, so treating as a missed cut on map containing big_cut
        cut_idx = next_cut_idx
        accumulated_frag_len += next_frag_len
    return accumulated_frag_len, cut_idx


def extend_aln(ref_it: OpticalMapIterator, query_it: OpticalMapIterator, params: ExtensionParams):
    """ extends in one direction defined by iterators """
    aligned_pairs: List[Tuple[int, int]] = []  # first: reference cut index, second: query cut index
    frags_do_match: Callable[[float, float], bool] = frags_match_functor(params.measurement_tol)
    n_mismatched = 0
    while ref_it.has_next() and query_it.has_next():  # while has next possible pair
        q_frag_len, q_cut_idx = query_it.next()
        r_frag_len, r_cut_idx = ref_it.next()

        # if fragments match exactly, add them to alignment extension and continue
        if frags_do_match(r_frag_len, q_frag_len):
            n_mismatched = 0
            aligned_pairs.append((r_cut_idx, q_cut_idx))
            continue

        # if fragments do not match, choose the longer one and try to append next fragments to the shorter one
        n_missed_cuts = 0
        if r_frag_len > q_frag_len:
            frag_len, new_q_cut = find_missed_cut(big_frag=r_frag_len, other_frag=q_frag_len, other_map_it=query_it)
            if new_q_cut:
                n_missed_cuts = new_q_cut - q_cut_idx
                q_cut_idx = new_q_cut
                q_frag_len = frag_len
        else:
            frag_len, new_r_cut = find_missed_cut(big_frag=q_frag_len, other_frag=r_frag_len, other_map_it=ref_it)
            if new_r_cut:
                n_missed_cuts = new_r_cut - r_cut_idx
                r_cut_idx = new_r_cut
                r_frag_len = frag_len

        if n_missed_cuts > params.max_miss_cuts:
            return aligned_pairs
        aligned_pairs.append((r_cut_idx, q_cut_idx))
        if not frags_do_match(q_frag_len, r_frag_len):
            n_mismatched += 1
        if n_mismatched == params.max_mismatch:
            return aligned_pairs

    return aligned_pairs


def extend_seed(ref: OpticalMap, query: OpticalMap, seed: Alignment, params: ExtensionParams,
                scale_factor: float) -> Alignment:
    # scaling
    ref = OpticalMap(ref.get_frags() * scale_factor)

    # extension
    r_last_cut, q_last_cut = seed.cut_pairs[-1]
    query_it, ref_it = FwdOMapIterator(query, q_last_cut), FwdOMapIterator(ref, r_last_cut)
    forward_extension = extend_aln(ref_it, query_it, params)

    r_first_cut, q_first_cut = seed.cut_pairs[0]
    back_query_it, back_ref_it = BackOMapIterator(query, q_first_cut), BackOMapIterator(ref, r_first_cut)
    backward_extension = extend_aln(back_ref_it, back_query_it, params)
    backward_extension.reverse()

    return Alignment.from_cut_pairs(backward_extension + seed.cut_pairs + forward_extension)


def extend_with_refinement(ref: OpticalMap, query: OpticalMap, seeds: List[Alignment],
                           params: RefinedExtensionParams) -> List[Tuple[Alignment, float]]:
    """
    :return: alignments and a scale factor for each alignment
    """
    if params.n_scales == 0:
        logging.info("number of scales is 0")
        return []

    if np.isclose(params.max_scale, params.min_scale):  # if only one scale factor is possible
        extensions = [extend_seed(ref, query, seed, params.extension_params, params.min_scale) for seed in seeds]
        return [(aln, params.min_scale) for aln in extensions]

    # if multiple scale factors are possible, perform refinement
    result: List[Tuple[Alignment, float]] = []
    scale_range: float = params.max_scale - params.min_scale
    scale_step: float = scale_range / params.n_scales
    for seed in seeds:
        best_scale: float = -1
        best_score: float = 0
        best_extension: Alignment = Alignment.from_cut_pairs([])

        def try_scale_factor(scale: float) -> Tuple[Alignment, float]:
            extended_seed: Alignment = extend_seed(ref, query, seed, params.extension_params, scale)
            extended_seed_score: float = score.score_partial(extended_seed, params.score_params)
            return extended_seed, extended_seed_score

        for scale_factor in np.arange(params.min_scale, params.max_scale, scale_step):
            extension, e_score = try_scale_factor(scale_factor)
            if e_score > best_score:
                best_scale, best_score, best_extension = scale_factor, e_score, extension

        if best_scale == -1 or not best_extension.cut_pairs:
            raise Exception("logic error, best scale must have been chosen while looping over feasible scale factors")

        # further refinement with narrowed range
        for scale_factor in np.arange(best_score - scale_range / 4, best_score + scale_range / 4, scale_step / 2):
            extension, e_score = try_scale_factor(scale_factor)
            if e_score > best_score:
                best_scale, best_score, best_extension = scale_factor, e_score, extension
        result.append((best_extension, best_scale))
    return result
